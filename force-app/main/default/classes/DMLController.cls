public inherited sharing class DMLController {
    
    public static void doDelete( SObject sObj ) {
        if ( !sObj.getSObjectType().getDescribe().isDeletable() ) {
            throw new VT_Exception('Insufficient delete access. ( Object: '+sObj.getSObjectType() +' )' );
        }

        delete sObj;
    }

    public static void doDelete( List<SObject> sObjs ) {
        if ( !sObjs.isEmpty() && !sObjs[0].getSObjectType().getDescribe().isDeletable() ) {
            throw new VT_Exception('Insufficient delete access. ( Object: '+sObjs[0].getSObjectType() +' )' );
        }

        delete sObjs;
    }
}
